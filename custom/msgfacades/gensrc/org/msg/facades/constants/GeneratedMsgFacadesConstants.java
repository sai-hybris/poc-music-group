/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Jun 12, 2014 6:51:29 PM                     ---
 * ----------------------------------------------------------------
 */
package org.msg.facades.constants;

/**
 * @deprecated use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedMsgFacadesConstants
{
	public static final String EXTENSIONNAME = "msgfacades";
	
	protected GeneratedMsgFacadesConstants()
	{
		// private constructor
	}
	
	
}

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2013 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package org.msg.core.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import org.msg.core.constants.MsgCoreConstants;
import org.msg.core.setup.CoreSystemSetup;

import org.apache.log4j.Logger;


/**
 * Don't use. User {@link CoreSystemSetup} instead.
 */
@SuppressWarnings("PMD")
public class MsgCoreManager extends GeneratedMsgCoreManager
{
	@SuppressWarnings("unused")
	private static Logger LOG = Logger.getLogger(MsgCoreManager.class.getName());

	public static final MsgCoreManager getInstance()
	{
		final ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (MsgCoreManager) em.getExtension(MsgCoreConstants.EXTENSIONNAME);
	}
}
